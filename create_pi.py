#!/usr/bin/env python3

import json
import subprocess
from PyInquirer import prompt, print_json
from clint.textui import progress, puts, colored
from bs4 import BeautifulSoup
import requests
import os
import pathlib
import sys
from shutil import copy
import crypt

process = subprocess.run("/usr/bin/lsblk --json -p -o NAME,MOUNTPOINT,SIZE".split(), capture_output=True, text=True)

blockdevices = json.loads(process.stdout)
#print(json.dumps(blockdevices, indent=2))

blocks = [f"{x['name']} ({x['size']})" for x in blockdevices['blockdevices'] if not 'mmcblk' in x['name']]
#print(json.dumps(blocks, indent=2))

if not blocks:
    print("No viable USB devices found.")
    sys.exit(1)

type_to_install_questions = [
        {
            'type': 'list',
            'name': 'install_type',
            'message': 'Select flavour of Pi',
            'choices': [
                'pi-vpn',
                'pwnagotchi',
            ]
        }
    ]
answers = prompt(type_to_install_questions)
install_type = answers['install_type']

if install_type == 'pwnagotchi':
    print('Unsupported flavor')
    sys.exit(-1)


#print(json.dumps(blockdevices, indent=2))
questions = [
        {
            'type': 'list',
            'name': 'dest_device',
            'message': 'Where to install Pi',
            'choices': blocks,
        },
        {
            'type': 'list',
            'name': 'dest_arch',
            'message': 'What type of Pi',
            'choices': ['32-bit', '64-bit'],
        },
        {   'type': 'input',
            'name': 'dest_node_id',
            'message': 'Node ID',
            'default': '1',
        },
        {
            'type': 'input',
            'name': 'dest_upstream_dns',
            'message': 'Upstream DNS Server',
            'default': '172.16.0.1',
        },
        {
            'type': 'password',
            'name': 'dest_password',
            'message': 'User password',
        },
        {
            'type': 'confirm',
            'name': 'add_wifi_conf',
            'message': 'Add WiFi config',
        }
    ]

answers = prompt(questions)
if answers['add_wifi_conf']:
    wifi_questions = [
        {
            'type': 'input',
            'name': 'wifi_name',
            'message': 'WiFi Name',
        },
        {
            'type': 'input',
            'name': 'wifi_password',
            'message': 'WiFi Password',
        },
    ]
    wifi_answers = prompt(wifi_questions)
#    print(json.dumps(wifi_answers, indent=2))



dest_arch = 'arm64' if answers['dest_arch'] == '64-bit' else 'armhf'
image_name = f"raspios-lite-{dest_arch}.zip"
image_urls = {
        '32-bit': 'https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-11-08/2021-10-30-raspios-bullseye-armhf-lite.zip',
        '64-bit': 'https://downloads.raspberrypi.org/raspios_lite_arm64/images/raspios_lite_arm64-2021-11-08/2021-10-30-raspios-bullseye-arm64-lite.zip',
}
dest_password = answers['dest_password']
dest_node_id = answers['dest_node_id']
dest_upstream_dns = answers['dest_upstream_dns']

def get_cache_file(dest_arch, show_output=True):
    if show_output:
        print("[*] Checking for cache file") 
    cache_dir = f".cache/{dest_arch}"
    pathlib.Path(cache_dir).mkdir(parents=True, exist_ok=True)
    cache_files = os.listdir(cache_dir)
    cache_files.sort()
    if len(cache_files) > 0:
        if show_output:
            print("[+] Found cache file:", cache_files[-1])
        return f"{cache_dir}/{cache_files[-1]}"
    return None

def get_url_file(dest_arch):
    print("[*] Checking for dist file")
    base_url = f"https://downloads.raspberrypi.org/raspios_lite_{dest_arch}/images/"
    base_url = f"http://rynojvr-build.home.rynojvr.com/images/{dest_arch}/"
#    print(f"Fetching from {base_url}")
    html_text = requests.get(base_url).text
#    print(f"Got text: {html_text}")
    soup = BeautifulSoup(html_text, 'html.parser')
    #image_releases = [x['href'] for x in soup.find_all('a') if "Parent Directory" not in x and 'raspios' in x['href']]
    image_releases = [x['href'] for x in soup.find_all('a') if 'pi-vpn' in x['href']]
#    print(f"Image releases: {image_releases}")
    if len(image_releases) > 0:
        print("[+] Found dist file:", image_releases[-1])
        latest_image_release = image_releases[-1]
    else:
        print("[!] Unable to find dist file")
        return None
#    print(f"Latest release: {latest_image_release}")
#    latest_image_url = f"{base_url}{latest_image_release}"
#    print(f"latest image url: {latest_image_url}")
#    latest_image_html = requests.get(latest_image_url).text
#
#    soup = BeautifulSoup(latest_image_html, 'html.parser')
#    latest_image = [x['href'] for x in soup.find_all('a') if x['href'].endswith('.zip')][0]
#    print(f"latest_image_release: {latest_image_release}")
    return {
        'latest_image': f".cache/{dest_arch}/{latest_image_release}",
        'latest_image_name': latest_image_release,
        'latest_image_url': f"{base_url}{latest_image_release}",
    }

def get_file(arch, file_name, url):
    path = f".cache/{arch}/{file_name}"
    print(f"[*] Dist file not downloaded. Fetching {file_name}")
    r = requests.get(url, stream=True)
    with open(path, 'wb') as f:
        total_length = int(r.headers.get('content-length'))
        for chunk in progress.bar(r.iter_content(chunk_size=1024), expected_size=(total_length/1024) + 1):
            if chunk:
                f.write(chunk)
                f.flush()

cache_file = get_cache_file(dest_arch)
url_file = None
try:
    url_file = get_url_file(dest_arch)
except:
    print("[!] Unable to reach dist server")
    pass

#print(f"Cache file: {cache_file}")
#print(f"Url File: {url_file}")

if url_file:
    if cache_file != url_file['latest_image']:
        get_file(dest_arch, url_file['latest_image_name'], url_file['latest_image_url'])

#sys.exit(0)

#path = f".cache/{image_name}"
#if not os.path.exists(path):
#
#    # Fetching the image file 
#    # TODO: explore using tqdm (https://stackoverflow.com/a/53877507
#    # TODO: Add overrider for Raspi 4 (arm64)
#
#    print("Image not found in cache. Fetching...")
#else:
#    print("Found cached file")




cache_file = get_cache_file(dest_arch, False)
if not cache_file:
    print("[!] Unable to find file. check connection to rynojvr-build.home.rynojvr.com")
    sys.exit(1)

dest_device = answers['dest_device'].split()[0]

#print(f"cache file: {cache_file}")
#print(f"dest device: {dest_device}")

# mount & write them image file
print("Writing image to USB")
cmd_to_run = f"unzip -p {cache_file} | dd of={dest_device} bs=4M conv=fsync status=progress"
print(f"Command to run: {cmd_to_run}")
result = subprocess.run(cmd_to_run, shell=True) 
if result.returncode != 0:
    print("Error writing file to SD Card")
    sys.exit(1)

pathlib.Path('/mnt/usb1').mkdir(parents=True, exist_ok=True)
pathlib.Path('/mnt/usb2').mkdir(parents=True, exist_ok=True)

print("Mounting USB device 1")
result = subprocess.run(f"mount {dest_device}1 /mnt/usb1", shell=True)
if result.returncode != 0:
    print("Error mounting Pi partition")
    sys.exit(1)

print("Mounting USB device 2")
result = subprocess.run(f"mount {dest_device}2 /mnt/usb2", shell=True)
if result.returncode != 0:
    print("Error mounting Pi partition")
    sys.exit(1)

#dest_hostname = 'pi-cam'
dest_hostname = 'pi-vpn'

# update /etc/hostname
print("Updating /etc/hostname")
with open('/mnt/usb2/etc/hostname', 'w') as file:
    file.write(dest_hostname)

# update /etc/hosts
print("Updating /etc/hosts")
with open('/mnt/usb2/etc/hosts', 'r') as file:
    filedata = file.read()
filedata = filedata.replace('pi-vpn', dest_hostname)
with open('/mnt/usb2/etc/hosts', 'w') as file:
    file.write(filedata)

# update /etc/rc.local
print("Updating /etc/rc.local")
with open('/mnt/usb2/etc/rc.local', 'r') as file:
    filedata = file.read()
filedata = filedata.replace('exit 0', """
if [ -f /root/init.sh ]; then
    /bin/bash /root/init.sh &> /root/init.log
fi

exit 0
""")
with open('/mnt/usb2/etc/rc.local', 'w') as file:
    file.write(filedata)

print("Copying initial files")
copy('init.sh', '/mnt/usb2/root/')
#copy('init2.sh', '/mnt/usb2/root/')
copy('ansible-pull.service', '/mnt/usb2/etc/systemd/system/')
copy('/root/.ansible_user_pwd', '/mnt/usb2/root/')
copy('config.ini.template', '/mnt/usb2/root/config.ini')

print("Updating config.ini")
with open("/mnt/usb2/root/config.ini", 'r') as file:
    filedata = file.read()
filedata = filedata.replace('NODE_ID', dest_node_id)
filedata = filedata.replace('UPSTREAM_DNS', dest_upstream_dns) 
with open("/mnt/usb2/root/config.ini", 'w') as file:
    file.write(filedata)

# enable ssh
print("Enable SSH")
open('/mnt/usb1/ssh', 'a').close()

# create wifi config
if answers['add_wifi_conf']:
    print("Adding wifi config")
    copy('wpa_supplicant.conf', '/mnt/usb1/wpa_supplicant.conf')
    with open('/mnt/usb1/wpa_supplicant.conf', 'r') as file:
        filedata = file.read()
    filedata = filedata.replace('SSID_TO_REPLACE', wifi_answers['wifi_name'])
    filedata = filedata.replace('PSK_TO_REPLACE', wifi_answers['wifi_password'])
    with open('/mnt/usb1/wpa_supplicant.conf', 'w') as file:
        file.write(filedata)

with open('/mnt/usb2/root/init.sh', 'r') as file:
    filedata = file.read()
hashed_password = crypt.crypt(dest_password, crypt.mksalt(crypt.METHOD_SHA512))
filedata = filedata.replace('PASSWORD_TO_REPLACE', hashed_password)
with open('/mnt/usb2/root/init.sh', 'w') as file:
    file.write(filedata)

result = subprocess.run(f"umount /mnt/usb1", shell=True)
if result.returncode != 0:
    print("Error unmounting Pi partition")
    sys.exit(1)
result = subprocess.run(f"umount /mnt/usb2", shell=True)
if result.returncode != 0:
    print("Error unmounting Pi partition")
    sys.exit(1)

print("All done")

